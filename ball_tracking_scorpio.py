import cv2

range_min = (29, 86, 6)
range_max = (64, 255, 255)

cam = cv2.VideoCapture(0)

while True:
    (capture, frame) = cam.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    filter = cv2.inRange(hsv, range_min, range_max)
    filter = cv2.erode(filter, None, iterations=2)
    filter = cv2.dilate(filter, None, iterations=2)
    contours = cv2.findContours(filter.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        if radius > 10:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 4)
    cv2.imshow('Scorpio - detector', frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break

cam.release()
cv2.destroyAllWindows()
